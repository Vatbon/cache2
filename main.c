#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define SIZE 2048*1024
#define COUNT 32
#define TYPE size_t

#pragma GCC push_options
#pragma GCC optimize ("O0")
size_t run(TYPE* arr, size_t size){
	union ticks{
		unsigned long long t64;
		struct s32{ long th, tl;} t32;
	} start, end;
	size_t l;
	register int k;
	register size_t i;
	size_t tmp = UINT_MAX;
	double min;
	for (k = 0, i = 0; i < size; i++){
		k = arr[k];
	}

	for (l = 0; l < COUNT; l++){
		asm("rdtsc\n":"=a"(start.t32.th),"=d"(start.t32.tl));
	for (k = 0, i = 0; i < size; i++){
		k = arr[k];
	}
		asm("rdtsc\n":"=a"(end.t32.th),"=d"(end.t32.tl));
		tmp = (tmp > end.t64 - start.t64) ? (end.t64 - start.t64) : tmp;
	}
	min = tmp / size;
	return min;
}
#pragma GCC pop_options


int main(){
	size_t size = SIZE;
	size_t i, j, fragsize, elemcount, fragcount;
	for(fragcount = 1; fragcount <= 32; fragcount++){
		fragsize = size / sizeof(TYPE);
		TYPE *arr = (TYPE*)malloc(size * fragcount);
		elemcount = fragsize / fragcount;

		for(i = 0; i < fragcount; i++)
			for(j = 0; j < elemcount; j++)
				arr[i * fragsize + j] = (i + 1) * fragsize + j;

		for(j = 0; j < elemcount - 1; j++)
			arr[(fragcount - 1) * fragsize + j] = j + 1;
		arr[(fragcount - 1) * fragsize + j] = 0;

		printf("%ld\n", run(arr, fragsize * fragcount));

		//free(arr);
	}
	return 0;
}
